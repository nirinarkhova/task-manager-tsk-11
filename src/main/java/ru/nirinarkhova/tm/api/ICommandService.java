package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}

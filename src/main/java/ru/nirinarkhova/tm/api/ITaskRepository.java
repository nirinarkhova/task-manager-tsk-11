package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
